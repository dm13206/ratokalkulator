<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Kalkulator rat</title>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<style type="text/css">
body {
	background-image: url("http://szuflandia.pjwstk.edu.pl/~s13206/patterns/gplaypattern.png");
	padding-left: 150px; padding-right:150px;
	font-family: Arial;
    font-size: 16px;
}
table, td {
   	padding: 5px;
	font-family: Arial;
	font-size: 15px;
}
h1 {
	font-family: Arial;
    font-size: 22px;
}
</style>
</head>
<body>
<div style="padding: 50px; margin-left: 10 px; margin-right: 10px; margin-top: 80px; margin-bottom: 80px; border-radius: 10px; text-align: left; background-color: #FEFEFE">
<FORM action="RatoServlet" method="get">
<table>
<tr>
	<td>
		<h1>Oblicz ratę kredytu</h1>
	</td>
</tr>
<tr>
	<td>Kwota kredytu</td>
	<td><input type="text" name="amount"></td>
</tr>
<tr>
	<td>Ilośc rat</td>
	<td><input type="text" name="rateCount"></td>
</tr>
<tr>
	<td>Oprocentowanie</td>
	<td><input type="text" name="interestRate"></td>
</tr>
<tr>
	<td>Opłata stała</td>
	<td><input type="text" name="fixedFee"></td>
</tr>

<tr>
	<td>Rodzaj rat:</td>
	<td><input type="radio" name="rateType" value="decreasing">malejąca</td>
	<td><input type="radio" name="rateType" value="fixed">stała</td>
</tr>
<tr>
	<td>&nbsp;</td>
	<td><input type="submit" value="Wyślij" style="width: 100px; height: 30px; font-family: Arial; font-size: 14px"></td>
</tr>
</table>
</FORM>
</div>
</body>
</html>