package servlets;

import java.io.IOException;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet("/RatoServlet")
public class RatoServlet extends HttpServlet {

	private static final long serialVersionUID = 1L;
	public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {
		
		
		String amount = request.getParameter("amount");
		String rateCount = request.getParameter("rateCount");
		String interestRate = request.getParameter("interestRate");
		String fixedFee = request.getParameter("fixedFee");
		String rateType = request.getParameter("rateType");
		
		Double am=Double.parseDouble(amount);
		Double rC=Double.parseDouble(rateCount);
		Double iR=Double.parseDouble(interestRate);
		Double fF=Double.parseDouble(fixedFee);
		Double interest, capital, interestAmount, rate;
		interest=iR*0.01/12; //odsetki = oprocentowanie w skali roku/12
		capital=am/rC; //kapitał = kwota kredytu/ilośc miesięcy
		interestAmount=interest*am; //odsetki za dany miesiąc
		rate=capital+interestAmount; //całkowita kwota raty
		response.setContentType("text/html; charset=utf-8");
		response.getWriter().println("<table border=1><tr><td>Nr raty " +"</td><td>Kwota kapitału "+ "</td><td>Kwota odsetek" + "</td><td>Całkowita kwota raty</td></tr>");
		
		int i;
		for (i=1; i<rC+1; i++){
			
			response.setContentType("text/html");
			response.getWriter().println("<tr><td>" + i + "</td><td>" + capital + "</td><td>" + interestAmount + "</td><td>" + rate + "</td></tr>");
			am=am-capital;
			interestAmount = interest*am;
			rate=capital+interestAmount;
		}
		response.getWriter().println("</table>");
		
	} 
	
	public void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException {
		
		
		String amount = request.getParameter("amount");
		String rateCount = request.getParameter("rateCount");
		String interestRate = request.getParameter("interestRate");
		String fixedFee = request.getParameter("fixedFee");
		String rateType = request.getParameter("rateType");
		
		Double am=Double.parseDouble(amount);
		Double rC=Double.parseDouble(rateCount);
		Double iR=Double.parseDouble(interestRate);
		Double fF=Double.parseDouble(fixedFee);
		Double interest, capital, interestAmount, rate;
		interest=iR*0.01/12; //odsetki = oprocentowanie w skali roku/12
		capital=am/rC; //kapitał = kwota kredytu/ilośc miesięcy
		interestAmount=interest*am; //odsetki za dany miesiąc
		rate=capital+interestAmount; //całkowita kwota raty
		response.setContentType("text/html; charset=utf-8");
		response.getWriter().println("<table border=1><tr><td>Nr raty " +"</td><td>Kwota kapitału "+ "</td><td>Kwota odsetek" + "</td><td>Całkowita kwota raty</td></tr>");
		
		int i;
		for (i=1; i<rC+1; i++){
			
			response.setContentType("text/html");
			response.getWriter().println("<tr><td>" + i + "</td><td>" + capital + "</td><td>" + interestAmount + "</td><td>" + rate + "</td></tr>");
			am=am-capital;
			interestAmount = interest*am;
			rate=capital+interestAmount;
		}
		response.getWriter().println("</table>");
		
		
		
	}
}
