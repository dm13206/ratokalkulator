package org.apache.jsp;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;

public final class index_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {

  private static final JspFactory _jspxFactory = JspFactory.getDefaultFactory();

  private static java.util.Vector _jspx_dependants;

  private org.glassfish.jsp.api.ResourceInjector _jspx_resourceInjector;

  public Object getDependants() {
    return _jspx_dependants;
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    PageContext pageContext = null;
    HttpSession session = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;

    try {
      response.setContentType("text/html; charset=UTF-8");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;
      _jspx_resourceInjector = (org.glassfish.jsp.api.ResourceInjector) application.getAttribute("com.sun.appserv.jsp.resource.injector");

      out.write("\n");
      out.write("<!DOCTYPE html PUBLIC \"-//W3C//DTD HTML 4.01 Transitional//EN\" \"http://www.w3.org/TR/html4/loose.dtd\">\n");
      out.write("<html>\n");
      out.write("<head>\n");
      out.write("<meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\">\n");
      out.write("<title>Kalkulator rat</title>\n");
      out.write("\t<meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\">\n");
      out.write("<style type=\"text/css\">\n");
      out.write("body {\n");
      out.write("\tbackground-image: url(\"http://szuflandia.pjwstk.edu.pl/~s13206/patterns/gplaypattern.png\");\n");
      out.write("\tpadding-left: 150px; padding-right:150px;\n");
      out.write("\tfont-family: Arial;\n");
      out.write("    font-size: 16px;\n");
      out.write("}\n");
      out.write("table, td {\n");
      out.write("   \tpadding: 5px;\n");
      out.write("\tfont-family: Arial;\n");
      out.write("\tfont-size: 15px;\n");
      out.write("}\n");
      out.write("h1 {\n");
      out.write("\tfont-family: Arial;\n");
      out.write("    font-size: 22px;\n");
      out.write("}\n");
      out.write("</style>\n");
      out.write("</head>\n");
      out.write("<body>\n");
      out.write("<div style=\"padding: 50px; margin-left: 10 px; margin-right: 10px; margin-top: 80px; margin-bottom: 80px; border-radius: 10px; text-align: left; background-color: #FEFEFE\">\n");
      out.write("<FORM action=\"RatoServlet\" method=\"get\">\n");
      out.write("<table>\n");
      out.write("<tr>\n");
      out.write("\t<td>\n");
      out.write("\t\t<h1>Oblicz ratę kredytu</h1>\n");
      out.write("\t</td>\n");
      out.write("</tr>\n");
      out.write("<tr>\n");
      out.write("\t<td>Kwota kredytu</td>\n");
      out.write("\t<td><input type=\"text\" name=\"amount\"></td>\n");
      out.write("</tr>\n");
      out.write("<tr>\n");
      out.write("\t<td>Ilośc rat</td>\n");
      out.write("\t<td><input type=\"text\" name=\"rateCount\"></td>\n");
      out.write("</tr>\n");
      out.write("<tr>\n");
      out.write("\t<td>Oprocentowanie</td>\n");
      out.write("\t<td><input type=\"text\" name=\"interestRate\"></td>\n");
      out.write("</tr>\n");
      out.write("<tr>\n");
      out.write("\t<td>Opłata stała</td>\n");
      out.write("\t<td><input type=\"text\" name=\"fixedFee\"></td>\n");
      out.write("</tr>\n");
      out.write("\n");
      out.write("<tr>\n");
      out.write("\t<td>Rodzaj rat:</td>\n");
      out.write("\t<td><input type=\"radio\" name=\"rateType\" value=\"decreasing\">malejąca</td>\n");
      out.write("\t<td><input type=\"radio\" name=\"rateType\" value=\"fixed\">stała</td>\n");
      out.write("</tr>\n");
      out.write("<tr>\n");
      out.write("\t<td>&nbsp;</td>\n");
      out.write("\t<td><input type=\"submit\" value=\"Wyślij\" style=\"width: 100px; height: 30px; font-family: Arial; font-size: 14px\"></td>\n");
      out.write("</tr>\n");
      out.write("</table>\n");
      out.write("</FORM>\n");
      out.write("</div>\n");
      out.write("</body>\n");
      out.write("</html>");
    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          out.clearBuffer();
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
        else throw new ServletException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }
}
